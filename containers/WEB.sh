cd ../
if [ -d "web" ] 
then
    echo "Directory /web exists." 
else
    echo "Initialize"
    git clone git@bitbucket.org:skinlot/web.git web
fi

cd web/
git fetch -q git@bitbucket.org:skinlot/web.git master
git checkout master
git pull -q git@bitbucket.org:skinlot/web.git master
